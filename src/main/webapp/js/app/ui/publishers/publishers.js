/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
altogamer.ui.publishers = (function() {

    function init() {
        $("#publishersList").altogamerInfiniteScrollList().on("agInfiniteScrollItemsRender", function() {
            $(document).trigger("thumbAdded");
        });

        altogamer.ui.publishers.contextMenu.init();
    }

    return {
        init: init
    };

}());



altogamer.ui.publishers.contextMenu = (function() {
    var $currentItem;

    function init() {
        if (typeof agUser === "undefined") {
            return;
        }

        $("#editPublisherModal").on("submit", "#editPublisherForm", onSubmitEditPublisherForm);

        $(document).contextMenu({
            selector: ".js-ag-edit-publisher-context-menu",
            callback: function(menuKey) {
                $currentItem = $(this);
                var publisher = {
                    id: $currentItem.data("id"),
                    name: $currentItem.data("name"),
                    imageUrlCover: $currentItem.data("imageUrlCover")
                };
                switch (menuKey) {
                    case "edit":
                        showEditPublisherModal(publisher);
                        break;
                    case "search":
                        searchImages(publisher);
                        break;
                }
            },
            items: {
                "edit": {name: "<span class='glyphicon glyphicon-pencil'></span> Editar imagen del distribuidor"},
                "search": {name: "<span class='glyphicon glyphicon-search'></span> Buscar imágenes..."},
            }
        });
    }

    function searchImages(game) {
        var keywords = game.name + " logo";
        var url = "https://www.google.com/search?as_st=y&tbm=isch&hl=en&as_q=" + encodeURIComponent(keywords) + "&as_epq=&as_oq=&as_eq=&cr=&as_sitesearch=&safe=images&tbs=isz:m#as_st=y&hl=en&q=" + encodeURIComponent(keywords) + "&tbm=isch&tbs=isz:m&imgdii=_";
        window.open(url);
    }

    function onSubmitEditPublisherForm() {
        var $form = $(this);
        var publisher = {
            id: $form.find("#editPublisherForm_id").val(),
            imageUrlCover: $form.find("#editPublisherForm_imageUrlCover").val()
        };

        altogamer.service.admin.updatePublisher(publisher)
                .success(function() {
                    $currentItem.data("imageUrlCover", publisher.imageUrlCover);
                    altogamer.ui.layout.defaultLayout.imageLoader.loadImageInItem($currentItem, publisher.imageUrlCover);
                    hideEditPublisherModal();
                })
                .error(function() {
                    alert("Ups, ocurrio un error guardando el juego...");
                });

        return false;
    }

    function showEditPublisherModal(publisher) {
        $("#editPublisherModal").html($("#editPublisherModalTemplate").render(publisher));
        $("#editPublisherModal").modal("show");
    }

    function hideEditPublisherModal() {
        $("#editPublisherModal").modal("hide");
    }

    return {
        init: init
    };
})();


$(document).ready(function() {
    altogamer.ui.publishers.init();
});

