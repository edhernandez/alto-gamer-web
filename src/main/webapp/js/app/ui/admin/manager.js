/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
altogamer.ui.gamesManager = (function() {

    var games;
    var selectedGame;

    function init() {
        initSearchbox();
        initSelectGame();
        initSynonym();
    }

    function initSearchbox() {
        var $searchTextInput = $("#ag-manager-search-input");
        $searchTextInput.focus();
        $("#ag-manager-search-form").submit(function() {
            var keywords = $searchTextInput.val();
            altogamer.service.game.findByKeywords(keywords).done(function(data) {
                games = data;
                renderGames(games);
            });
            return false;
        });

    }

    function initSelectGame() {
        $("#ag-manager-games").on("click", ".js-ag-select-game", selectGame);
    }

    function initSynonym() {
        $("#ag-manager-games").on("click", ".js-ag-synonym-add", onAddSynonym);
        $("#ag-manager-selected-game-synonyms").on("click", ".js-ag-synonym-delete", onDeleteSynonym);
    }

    function onAddSynonym(event) {
        var $target = $(event.currentTarget);
        if (!selectedGame) {
            alert("Primero tenés que seleccionar el juego padre");
            return false;
        }
        if (!$target.hasClass("text-muted")) {
            var gameSynonym = {
                alias: selectedGame.alias,
                synonym: $target.data("alias")
            };
            if (gameSynonym.alias !== gameSynonym.synonym) {
                altogamer.service.admin.addSynonym(gameSynonym)
                        .success(function() {
                            $target.parent().parent().hide("slow");
                            renderSelectedGame(selectedGame);
                        })
                        .error(function() {
                            alert("Ocurrio un error al intentar agregar un sinonimo");
                        });
            }
            else {
                //its the selected game, ignore
                $target.addClass("text-muted");
            }
        }
        return false;
    }

    function onDeleteSynonym(event) {
        var $target = $(event.currentTarget);
        var id = $target.data("id");
        altogamer.service.admin.deleteSynonym(id)
                .success(function() {
                    $target.parent().parent().hide("slow");
                })
                .error(function() {
                    alert("Ocurrio un error al intentar eliminar un sinonimo");
                });
        return false;
    }

    function renderGames(games) {
        $("#ag-manager-games").html($("#gameListTemplate").render(games));
    }

    function renderSelectedGame(game) {
        $("#ag-manager-selected-game").html($("#selectedGameTemplate").render(game));
        $("#ag-manager-selected-game-synonyms").empty();
        altogamer.service.admin.findSynonymByAlias(game.alias).success(function(synonyms) {
            $("#ag-manager-selected-game-synonyms").html($("#selectedGameSynonymTemplate").render(synonyms));
        });
    }

    function selectGame(event) {
        var $target = $(event.currentTarget);
        var index = $target.data("gameIndex");
        selectedGame = games[index];
        renderSelectedGame(selectedGame);

        $("#ag-manager-games tr").removeClass("success");
        $target.addClass("success");

        return false;
    }


    return {
        init: init
    };

}());

$(document).ready(function() {
    altogamer.ui.gamesManager.init();
});