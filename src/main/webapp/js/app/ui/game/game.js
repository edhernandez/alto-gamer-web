/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
altogamer.ui.game = (function() {
    var currentGame;

    function init(game) {
        setCurrentGame(game);
        initMainImage();
        initScreenshots();
        initVideos();
        initAdmin();
    }

    function initVideos() {
        $("#videos").altogamerYouTube({
            query: currentGame.name + " videogame gameplay",
            width: "100%",
            height: 450,
            maxResults: 5
        });
    }

    function initMainImage() {
        var searchOptions = {
            query: currentGame.name + " screenshot",
            maxResults: 1,
            size: "xxlarge" //commendedSizeForSearch(currentGame) //icon, small, medium, large, xlarge, xxlarge, huge
        };
        var $imageContainer = $("#gameMainImage");

        //set height of imageContainer to use available viewport space.
        var offsetTop = $imageContainer.offset().top;
        var windowHeight = $(window).height();
        var imageFullHeight = windowHeight - offsetTop;
        $imageContainer.height(imageFullHeight + "px");

        altogamer.service.image.findByKeywords(searchOptions)
                .done(function(data) {
                    var image = data.responseData.results[0];
                    if (image && image.url) {
                        altogamer.service.image.setImageAsBackground($imageContainer, image.url);
                    }
                });

        $(document).trigger("scoreAdded");
    }

    function recommendedSizeForSearch(game) {
        var size;
        var releaseYear = game.releaseDate ? game.releaseDate.year() : 1999;
        if (releaseYear < 1997) {
            size = "large";
        } else if (releaseYear < 2000) {
            size = "xlarge";
        } else if (releaseYear < 2007) {
            size = "xxlarge";
        } else {
            size = "huge";
        }
        return size;
    }

    function initScreenshots() {
        var searchOptions = {
            query: currentGame.name + " screenshot",
            maxResults: 6, //1-8
            size: recommendedSizeForSearch(currentGame) //icon, small, medium, large, xlarge, xxlarge, huge
        };
        var waypointTriggered = false;

        altogamer.service.image.findByKeywords(searchOptions)
                .done(function(data) {
                    if (data.responseData && data.responseData.results) {
                        $("#screenshots").html($("#screenshotsTemplate").render(data.responseData.results));
                        $(".js-ag-screenshot").colorbox({
                            rel: "js-ag-screenshot",
                            title: currentGame.name,
                            transition: "none",
                            maxHeight: "90%",
                            maxWidth: "90%"
                        });
                        if (waypointTriggered) {
                            $(document).trigger("thumbAdded");
                        }
                    }
                });

        $("#screenshots").waypoint(function() {
            $(document).trigger("thumbAdded");
            waypointTriggered = true;
        }, {offset: "80%", triggerOnce: true});
    }

    function setCurrentGame(game) {
        currentGame = game;
        if (currentGame.releaseDate) {
            currentGame.releaseDate = moment(currentGame.releaseDate, "YYYY-MM-DD");
        }
    }

    function initAdmin() {
        $("#featureGame").on("click", function() {
            altogamer.service.admin.changeFeatured($(this).data("id"))
                    .success(featureGameSuccess)
                    .fail(featureGameError);
        });
        $("#addSynonymButton").on("click", function() {
            altogamer.ui.layout.defaultLayout.modal.showAddSynonymModal(currentGame);
        });
    }

    function featureGameSuccess() {
        $("#alertControlPanel").removeClass("alert-info");
        $("#alertControlPanel").addClass("alert-success");
        $("#alertControlPanel").html("Cambio guardado.");
    }

    function featureGameError() {
        $("#alertControlPanel").removeClass("alert-info");
        $("#alertControlPanel").addClass("alert-danger");
        $("#alertControlPanel").html("ERROR guardando el cambio.");
    }

    return {
        init: init
    };
}());

$(document).ready(function() {
    altogamer.ui.game.init(pageCurrentGame);
});
