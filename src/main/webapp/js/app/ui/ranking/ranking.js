/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
altogamer.ui.ranking = (function() {

    function init() {
        $("#gameList").altogamerInfiniteScrollList().on("agInfiniteScrollItemsRender", function () {
            $(document).trigger("thumbAdded");
            $(document).trigger("scoreAdded");
        });
    }

    return {
        init: init
    };

}());

$(document).ready(function() {
    altogamer.ui.ranking.init();
});

