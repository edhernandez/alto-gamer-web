/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
altogamer.service.game = (function() {

    var SEARCH_URL = altogamer.baseURI + "api/search/";
    var GAME_URL = altogamer.baseURI + "juegos/";

    function findByKeywords(description) {
        var url = SEARCH_URL + encodeURIComponent(description);
        return $.getJSON(url);
    }
    /**
     * Redirect browser to a new game page.
     */
    function goTo(platform, alias) {
        location.href = GAME_URL + platform + "/" + alias;
    }

    return {
        findByKeywords: findByKeywords,
        goTo: goTo
    };

}());
