/*  This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
altogamer.service.image = (function() {

    /* Known domains that do not allow linking. */
    var BLACKLIST = [
        "1.bp.blogspot.com",
        "gry-online.pl",
        "image.dosgamesarchive.com",
        "screens.latestscreens.com",
        "www.bluesnews.com",
        "www.consolegames.ro",
        "www.dsogaming.com",
        "www.gamesaktuell.de",
        "www.gamezone.de",
        "www.old-games.com",
        "www.pcgames.de",
        "www.pcgameshardware.com"
    ];

    /*
     * Google API documentation: https://developers.google.com/image-search/v1/jsondevguide?csw=1#json_args
     */
    function findByKeywords(options) {
        var settings = $.extend({
            query: "Bioshock",
            maxResults: undefined, //1-8
            size: undefined //icon, small, medium, large, xlarge, xxlarge, huge
        }, options);
        var maxResults = 8; //bring as many results as possible, then slice it.
        var url = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + encodeURIComponent(settings.query);
        url += settings.maxResults ? "&rsz=" + maxResults : "";
        url += settings.size ? "&imgsz=" + settings.size : "";

        return $.ajax({
            url: url,
            type: "GET",
            dataType: "jsonp"
        }).then(function (data) {
            return filterByDomain(data, BLACKLIST);
        }).then(function (data) {
            return filterByMaxResults(data, settings.maxResults);
        });
    }

    /** Filters an array of images, checking if the URL is blacklisted.
     * @return Array a new Array with the filtered results.
     */
    function filterByDomain(data, blacklist) {
        var i, j, image;
        var filteredResults = [];
        var images;
        if (data && data.responseData) {
            images = data.responseData.results;
            for (i = 0; i < images.length; i++) {
                image = images[i];
                if (containsAny(image.url, blacklist) === -1) {
                    filteredResults.push(image);
                }
            }
            data.responseData.results = filteredResults;
        }
        return data;
    }

    /** Filters the array and slices it to maxResults.
     */
    function filterByMaxResults(data, maxResults) {
        if (maxResults && data.responseData && data.responseData.results) {
            data.responseData.results = data.responseData.results.slice(0, maxResults);
        }
        return data;
    }

    /** Checks if any of the strings in an array is contained in the sepecified string.
     * @return int the index in the array that is contained in the string, -1 if no matches are found.
     */
    function containsAny(str, strArray) {
        for (var j = 0; j < strArray.length; j++) {
            if (str.indexOf(strArray[j]) !== -1)
                return j;
        }
        return -1;
    }

    /** Prealods the image, and when loading is finished sets the image as
     *   background of the given container.
     */
    function setImageAsBackground($item, imageUrl) {
        var image = new Image();
        image.src = imageUrl;
        $(image).one("load", function() {
            $item.css("border", "none");
            $item.css("background-size", "cover");
            $item.css("background-repeat", "no-repeat");
            $item.css("background-position", "center");
            $item.css("background-image", "url('" + imageUrl + "')");
            $item.css("opacity", "1");
        }).each(function() {
            if (this.complete)
                $(this).load();
        });
    }

    return {
        findByKeywords: findByKeywords,
        setImageAsBackground: setImageAsBackground
    };

}());
