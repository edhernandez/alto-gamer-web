<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="ag-ranking-navbar">
                <a href="<c:url value="/ranking"/>">Alto Ranking</a>
                <a href="<c:url value="/ranking/2014"/>">2014</a>
                <a href="<c:url value="/ranking/2013"/>">2013</a>
                <a href="<c:url value="/ranking/2012"/>">2012</a>
                <a href="<c:url value="/ranking/2011"/>">2011</a>
                <a href="<c:url value="/ranking/2010"/>">2010</a>
                <a href="<c:url value="/ranking/2009"/>">2009</a>
                <a href="<c:url value="/ranking/2008"/>">2008</a>
                <a href="<c:url value="/ranking/2007"/>">2007</a>
                <a href="<c:url value="/ranking/2006"/>">2006</a>
                <a href="<c:url value="/ranking/2005"/>">2005</a>
                <a href="<c:url value="/ranking/2004"/>">2004</a>
                <a href="<c:url value="/ranking/2003"/>">2003</a>
                <a href="<c:url value="/ranking/2002"/>">2002</a>
                <a href="<c:url value="/ranking/2001"/>">2001</a>
                <a href="<c:url value="/ranking/2000"/>">2000</a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="ag-title-section">
                <h2><c:out value="${sectionTitle1}"/></h2>
                <h3><c:out value="${sectionTitle2}"/></h3>
            </div>
        </div>
    </div>

    <div class="row" id="gameList" data-ag-next-page-url="<c:out value="${url}"/>"></div>

    <div class="row">
        <div class="col-md-12">
            <p id="agLoadMoreItems" class="ag-spinner">
                <span class="glyphicon glyphicon-refresh"></span> Cargando juegos...
            </p>
        </div>
    </div>

</div>


<%--********************************************************************
                               TEMPLATES
*********************************************************************--%>
<script id="agInfiniteScrollListTemplate" type="text/x-jsrender">
    <div class="col-md-4">
        <a class="ag-release" href="<c:url value="/"/>juegos/{{:platform}}/{{:alias}}">
            <div class="ag-release-thumb-container">
                <div class="js-ag-release-thumb js-ag-edit-context-menu ag-release-thumb" data-id="{{:id}}" data-search-keywords="{{>name}} game" data-image-url-cover="{{>imageUrlCover}}"></div>
                <div class="ag-review-list">
                    <span class="ag-score-box ag-score-box-news ag-js-score-box ag-data-score"><span class="ag-js-score-points" data-score="{{>scoreAltogamer}}">{{>scoreAltogamer}}</span></span>
                </div>
            </div>
            <h1>{{>name}}</h1>
        </a>
    </div>
</script>