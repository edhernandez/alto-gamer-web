<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<div class="ag-game-main">
    <div id="gameMainImage" class="js-ag-no-image-cover js-ag-edit-context-menu ag-game-main-image" data-id="<c:out value="${game.id}"/>"></div>
    <div id="gameNavbar" class="ag-game-main-bar">
        <div class="container">
            <div class="col-md-6">
                <div class="ag-game-header">
                    <h1>${game.name}</h1>
                    <span>${game.platform}</span>
                    <c:if test="${not empty game.publisher}">
                        <span class="glyphicon glyphicon-minus"></span>
                        <span><a href="<c:url value="/"/>distribuidoras/${game.publisher}">${game.publisher}</a></span>
                        </c:if>
                        <c:if test="${not empty game.releaseDate}">
                        <span class="glyphicon glyphicon-minus"></span>
                        <span><fmt:formatDate type="date" dateStyle="long" value="${game.releaseDate}"/></span>
                    </c:if>
                </div>
            </div>
            <div class="col-md-6 ag-game-scores">
                <c:if test="${not empty game.scoreVideogamer}">
                    <a class="ag-score-box ag-js-score-box" href="${game.urlVideogamer}" target="_blank">
                        <span class="ag-score-points ag-js-score-points" data-score="${game.scoreVideogamer}">${game.scoreVideogamer}</span>
                        <span class="ag-score-source">Videogamer</span>
                    </a>
                </c:if>
                <c:if test="${not empty game.scorePcgamer}">
                    <a class="ag-score-box ag-js-score-box" href="${game.urlPcgamer}" target="_blank">
                        <span class="ag-score-points ag-js-score-points" data-score="${game.scorePcgamer}">${game.scorePcgamer}</span>
                        <span class="ag-score-source">PC Gamer</span>
                    </a>
                </c:if>
                <c:if test="${not empty game.scoreMetacritic}">
                    <a class="ag-score-box ag-js-score-box" href="${game.urlMetacritic}" target="_blank">
                        <span class="ag-score-points ag-js-score-points" data-score="${game.scoreMetacritic}">${game.scoreMetacritic}</span>
                        <span class="ag-score-source">Metacritic</span>
                    </a>
                </c:if>
                <c:if test="${not empty game.scoreIgn}">
                    <a class="ag-score-box ag-js-score-box" href="${game.urlIgn}" target="_blank">
                        <span class="ag-score-points ag-js-score-points" data-score="${game.scoreIgn}">${game.scoreIgn}</span>
                        <span class="ag-score-source">IGN</span>
                    </a>
                </c:if>
                <c:if test="${not empty game.scoreGamesradar}">
                    <a class="ag-score-box ag-js-score-box" href="${game.urlGamesradar}" target="_blank">
                        <span class="ag-score-points ag-js-score-points" data-score="${game.scoreGamesradar}">${game.scoreGamesradar}</span>
                        <span class="ag-score-source">GamesRadar</span>
                    </a>
                </c:if>
                <c:if test="${not empty game.scoreGamespot}">
                    <a class="ag-score-box ag-js-score-box" href="${game.urlGamespot}" target="_blank">
                        <span class="ag-score-points ag-js-score-points" data-score="${game.scoreGamespot}">${game.scoreGamespot}</span>
                        <span class="ag-score-source">Gamespot</span>
                    </a>
                </c:if>
                <c:if test="${not empty game.scoreDestructoid}">
                    <a class="ag-score-box ag-js-score-box" href="${game.urlDestructoid}" target="_blank">
                        <span class="ag-score-points ag-js-score-points" data-score="${game.scoreDestructoid}">${game.scoreDestructoid}</span>
                        <span class="ag-score-source">Destructoid</span>
                    </a>
                </c:if>
                <c:if test="${not empty game.scoreAltogamer}">
                    <div class="ag-score-box ag-score-altogamer ag-js-score-box">
                        <span class="ag-score-points ag-js-score-points" data-score="${game.scoreAltogamer}">${game.scoreAltogamer}</span>
                        <span class="ag-score-source">Alto Gamer</span>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div id="screenshots" class="row"></div>

    <div class="row">
        <div class="col-md-8">
            <div id="videos" class="ag-videos"></div>

            <h2>Tu opini�n</h2>
            <div class="fb-comments" data-href="${pageUrl}" data-numposts="20"></div>
        </div>

        <div class="col-md-4">
            <div class="ag-content-box ag-content-box-info">
                <div class="ag-content-box-canvas">
                    <ul>
                        <li><strong>T�tulo: </strong>${game.name}</li>
                            <c:if test="${not empty game.releaseDate}">
                            <li><strong>Fecha: </strong><fmt:formatDate type="date" dateStyle="long" value="${game.releaseDate}"/></li>
                            </c:if>
                            <c:if test="${not empty game.publisher}">
                            <li><strong>Publicador: </strong><a href="<c:url value="/"/>distribuidoras/${game.publisher}">${game.publisher}</a></li>
                            </c:if>
                    </ul>
                </div>
            </div>

            <c:if test="${not empty game.priceSteam || not empty game.priceGamersgate || not empty game.priceGog}">
                <div class="ag-content-box ag-content-box-success">
                    <div class="ag-content-box-canvas">
                        <c:if test="${not empty game.priceSteam}">
                            <ul>
                                <li><strong>Precio en Steam: </strong>
                                    <a href="${game.urlSteam}" target="_blank">
                                        ${game.priceSteam > 0 ? "USD ".concat(game.priceSteam) : "Gratis"}
                                    </a>
                                </li>
                            </ul>
                        </c:if>
                        <c:if test="${not empty game.priceGamersgate}">
                            <ul>
                                <li><strong>Precio en Gamersgate: </strong>
                                    <a href="${game.urlGamersgate}" target="_blank">
                                        ${game.priceGamersgate > 0 ? "USD ".concat(game.priceGamersgate) : "Gratis"}
                                    </a>
                                </li>
                            </ul>
                        </c:if>
                        <c:if test="${not empty game.priceGog}">
                            <ul>
                                <li><strong>Precio en GOG: </strong>
                                    <a href="${game.urlGog}" target="_blank">
                                        ${game.priceGog > 0 ? "USD ".concat(game.priceGog) : "Gratis"}
                                    </a>
                                </li>
                            </ul>
                        </c:if>
                    </div>
                </div>
            </c:if>

            <div class="fb-share-button" data-href="http://www.altogamer.com/juegos/PC/${game.alias}" data-type="button_count"></div>

            <h2>Puntajes y cr�ticas</h2>
            <ul class="ag-review-list">
                <c:if test="${not empty game.scoreAltogamer}">
                    <li class="ag-review-altogamer">
                        <span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreAltogamer}">${game.scoreAltogamer}</span></span>
                        Alto Gamer
                    </li>
                </c:if>
                <c:if test="${not empty game.urlMetacritic}">
                    <li>
                        <c:if test="${not empty game.scoreMetacritic}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreMetacritic}">${game.scoreMetacritic}</span></span></c:if>
                        <c:if test="${empty game.scoreMetacritic}"><span class="ag-score-box ag-score-zero"><span>--</span></span></c:if>
                        <a href="${game.urlMetacritic}" target="_blank">Metacritic</a>
                    </li>
                </c:if>
                <c:if test="${not empty game.urlDestructoid}">
                    <li>
                        <c:if test="${not empty game.scoreDestructoid}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreDestructoid}">${game.scoreDestructoid}</span></span></c:if>
                        <c:if test="${empty game.scoreDestructoid}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="0">--</span></span></c:if>
                        <a href="${game.urlDestructoid}" target="_blank">Destructoid</a>
                    </li>
                </c:if>
                <c:if test="${not empty game.urlGamespot}">
                    <li>
                        <c:if test="${not empty game.scoreGamespot}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreGamespot}">${game.scoreGamespot}</span></span></c:if>
                        <c:if test="${empty game.scoreGamespot}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="0">--</span></span></c:if>
                        <a href="${game.urlGamespot}" target="_blank">Gamespot</a>
                    </li>
                </c:if>
                <c:if test="${not empty game.urlGamesradar}">
                    <li>
                        <c:if test="${not empty game.scoreGamesradar}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreGamesradar}">${game.scoreGamesradar}</span></span></c:if>
                        <c:if test="${empty game.scoreGamesradar}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="0">--</span></span></c:if>
                        <a href="${game.urlGamesradar}" target="_blank">Games Radar</a>
                    </li>
                </c:if>
                <c:if test="${not empty game.urlIgn}">
                    <li>
                        <c:if test="${not empty game.scoreIgn}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreIgn}">${game.scoreIgn}</span></span></c:if>
                        <c:if test="${empty game.scoreIgn}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="0">--</span></span></c:if>
                        <a href="${game.urlIgn}" target="_blank">IGN</a>
                    </li>
                </c:if>
                <c:if test="${not empty game.urlPcgamer}">
                    <li>
                        <c:if test="${not empty game.scorePcgamer}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scorePcgamer}">${game.scorePcgamer}</span></span></c:if>
                        <c:if test="${empty game.scorePcgamer}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="0">--</span></span></c:if>
                        <a href="${game.urlPcgamer}" target="_blank">PC Gamer</a>
                    </li>
                </c:if>
                <c:if test="${not empty game.urlVideogamer}">
                    <li>
                        <c:if test="${not empty game.scoreVideogamer}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="${game.scoreVideogamer}">${game.scoreVideogamer}</span></span></c:if>
                        <c:if test="${empty game.scoreVideogamer}"><span class="ag-score-box ag-js-score-box"><span class="ag-js-score-points" data-score="0">--</span></span></c:if>
                        <a href="${game.urlVideogamer}" target="_blank">Videogamer</a>
                    </li>
                </c:if>
            </ul>

            <sec:authorize access="hasRole('admin')">
                <h2>Panel de control</h2>
                <div id="alertControlPanel" class="alert alert-info">Bienvenido, <sec:authentication property="principal.username"/></div>
                <input id="featureGame" type="checkbox" data-id="${game.id}" ${game.featured ? "checked" : ""}> Destacado
                <a id="addSynonymButton" class="btn btn-info pull-right">Agregar sinonimo</a>
            </sec:authorize>

        </div>
    </div>
</div>


<%--********************************************************************
                               TEMPLATES
*********************************************************************--%>
<script id="screenshotsTemplate" type="text/x-jsrender">
    <div class="col-md-2">
        <a class="ag-release js-ag-screenshot" href="{{>url}}">
            <div class="ag-release-thumb-container">
                <div class="js-ag-release-thumb ag-release-thumb ag-release-thumb-small" data-image-url-cover="{{>url}}"></div>
            </div>
        </a>
    </div>
</script>

<script id="agYouTubeVideosTemplate" type="text/x-jsrender">
    {{if videos && videos.length > 0 }}
    <h2>Videos destacados</h2>
    <div class="ag-youtube-videos">
    {{for videos}}
        <iframe class="ag-youtube-video {{:#index==0 ? 'ag-youtube-selected' : ''}}" data-index="{{:#index}}" width="{{:#parent.parent.data.settings.width}}" height="{{:#parent.parent.data.settings.height}}" src="http://www.youtube.com/embed/{{>id}}?html5=1" frameborder="0" allowfullscreen></iframe>
        {{/for}}
        </div>
        <div class="ag-youtube-thumbs">
        {{for videos}}
        <img class="ag-youtube-thumb {{:#index==0 ? 'ag-youtube-selected' : ''}}" data-index="{{:#index}}" src='{{>thumbnail.sqDefault}}' alt="{{>title}}" />
        {{/for}}
        <span class="ag-youtube-strech"></span>
    </div>
    {{/if}}
</script>


<script>
    var pageCurrentGame = {
        name: '<spring:message text="${game.name}" javaScriptEscape="true"/>',
        alias: '<spring:message text="${game.alias}" javaScriptEscape="true"/>',
        releaseDate: '<fmt:formatDate value="${game.releaseDate}" pattern="YYYY-MM-dd" />'
    };
</script>