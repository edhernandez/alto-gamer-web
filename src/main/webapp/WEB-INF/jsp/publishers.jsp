<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="ag-title-section">
                <h2><c:out value="${sectionTitle1}"/></h2>
                <h3><c:out value="${sectionTitle2}"/></h3>
            </div>
        </div>
    </div>

    <div class="row" id="publishersList" data-ag-next-page-url="<c:out value="${url}"/>"></div>

    <div class="row">
        <div class="col-md-12">
            <p id="agLoadMoreItems" class="ag-spinner">
                <span class="glyphicon glyphicon-refresh"></span> Cargando distribuidoras...
            </p>
        </div>
    </div>

</div>


<%--********************************************************************
                               MODALS
*********************************************************************--%>
<sec:authorize access="hasRole('admin')">
    <div id="editPublisherModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editGameModalLabel" aria-hidden="true"></div>
</sec:authorize>



<%--********************************************************************
                               TEMPLATES
*********************************************************************--%>
<sec:authorize access="hasRole('admin')">
    <script id="editPublisherModalTemplate" type="text/x-jsrender">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h3>Editar distribuidora</h3>
                </div>
                <form id="editPublisherForm" class="form-horizontal">
                    <input type="hidden" id="editPublisherForm_id" name="id" value="{{:id}}"/>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Id</label>
                            <div class="col-sm-9"><p class="form-control-static">{{:id}}</p></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nombre</label>
                            <div class="col-sm-9"><p class="form-control-static">{{>name}}</p></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Imagen</label>
                            <div class="col-sm-9">
                                <input type="url" id="editPublisherForm_imageUrlCover" name="imageUrlCover" class="form-control" placeholder="URL de imagen de cubierta..." value="{{>imageUrlCover}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="Guardar"/>
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </script>
</sec:authorize>

<script id="agInfiniteScrollListTemplate" type="text/x-jsrender">
    <div class="col-md-3">
        <a class="ag-release" href="<c:url value="/"/>distribuidoras/{{:name}}">
            <div class="ag-release-thumb-container">
                <div class="js-ag-release-thumb js-ag-edit-publisher-context-menu ag-release-thumb" data-id="{{:id}}" data-name="{{>name}}" data-search-keywords="{{>name}} logo" data-image-url-cover="{{>imageUrlCover}}"></div>
            </div>
            <h1>{{>name}}</h1>
        </a>
    </div>
</script>