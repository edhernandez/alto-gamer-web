<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="ag-title-section">
                <h2><c:out value="${sectionTitle1}"/></h2>
                <h3><c:out value="${sectionTitle2}"/></h3>
            </div>
        </div>
    </div>

    <div class="row" id="gameList" data-ag-next-page-url="<c:out value="${url}"/>"></div>

    <div class="row">
        <div class="col-md-12">
            <p id="agLoadMoreItems" class="ag-spinner">
                <span class="glyphicon glyphicon-refresh"></span> Cargando juegos...
            </p>
        </div>
    </div>

</div>


<%--********************************************************************
                               TEMPLATES
*********************************************************************--%>
<script id="agInfiniteScrollListTemplate" type="text/x-jsrender">
    <div class="col-md-4">
        <a class="ag-release" href="<c:url value="/"/>juegos/{{:platform}}/{{:alias}}">
            <div class="ag-release-thumb-container">
                <div class="js-ag-release-thumb js-ag-edit-context-menu ag-release-thumb" data-id="{{:id}}" data-search-keywords="{{>name}} game" data-image-url-cover="{{>imageUrlCover}}"></div>
                <div class="ag-review-list">
                    <span class="ag-score-box ag-score-box-news ag-js-score-box ag-data-score"><span class="ag-js-score-points" data-score="{{>scoreAltogamer}}">{{>scoreAltogamer}}</span></span>
                </div>
            </div>
            <h1>{{>name}}</h1>
        </a>
    </div>
</script>