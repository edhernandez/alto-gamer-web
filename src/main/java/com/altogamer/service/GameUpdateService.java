/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.service;

import java.io.IOException;

/**
 * Service for updating the game information database.
 * The implementation of this class refreshs all game database using external
 * sources (like Metacritic).
 */
public interface GameUpdateService {

    /**
     * Refresh the game database using external sources.
     */
    void update();

    void createSitemap() throws IOException;
}
