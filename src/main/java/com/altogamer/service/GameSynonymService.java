/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.service;

import com.altogamer.domain.GameSynonym;
import java.util.List;

public interface GameSynonymService {

    GameSynonym addSynonym(GameSynonym gameSynonym);
    GameSynonym findBySynonym(String synonym);
    List<GameSynonym> findByAlias(String alias);
    void delete(long id);
}
