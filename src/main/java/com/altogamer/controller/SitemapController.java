/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.controller;

import com.altogamer.exception.ResourceNotFoundException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * A controller for sitemaps.
 */
@Controller
public class SitemapController {

    @Value("${sitemap.dir}")
    private String sitemapDirectory;

    @RequestMapping("/{filename:sitemap*.+}")
    public void showSitemap(@PathVariable String filename, HttpServletResponse response) throws IOException {
        File file = new File(sitemapDirectory + filename);
        if (!file.exists()) {
            throw new ResourceNotFoundException();
        }
        try (InputStream in = new FileInputStream(file)) {
            byte[] buffer = new byte[1024];
            int bytesRead;
            response.setHeader("Content-Type", "application/xml");
            OutputStream outputStream = response.getOutputStream();
            while ((bytesRead = in.read(buffer)) > 0) {
                outputStream.write(buffer, 0, bytesRead);
            }
        }
    }
}
