/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.controller;

import com.altogamer.domain.Publisher;
import com.altogamer.service.PublisherService;
import com.altogamer.vo.PageVo;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for Publishers.
 */
@RestController
public class PublisherRestController {

    @Autowired
    private PublisherService publisherService;

    @RequestMapping("/api/publishers")
    public PageVo<Publisher> showAll(ModelMap model, @RequestParam int page, HttpServletRequest request) {
        String nextPageUrl = request.getContextPath() + "/api/publishers?page=" + (page + 1);
        PageVo<Publisher> pageVo = new PageVo<>();
        pageVo.setItems(publisherService.findAll(page));
        pageVo.setNextPageUrl(nextPageUrl);
        return pageVo;
    }
}
