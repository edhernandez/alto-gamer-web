/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.vo;

public class PublisherVo {

    private Long id;
    private String imageUrlCover;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageUrlCover() {
        return imageUrlCover;
    }

    public void setImageUrlCover(String imageUrlCover) {
        this.imageUrlCover = imageUrlCover;
    }

}
