/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The main game domain object.
 */
@Entity
public class Game implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String alias;
    @Enumerated(EnumType.STRING)
    private Platform platform;
    @Temporal(TemporalType.TIMESTAMP)
    private Date releaseDate;
    private String publisher;
    private String imageUrlCover;

    private Integer scoreAltogamer;

    private Integer scoreMetacritic;
    private String urlMetacritic;

    private Double scoreDestructoid;
    private String urlDestructoid;

    private Double scoreGamespot;
    private String urlGamespot;

    private Double scoreGamesradar;
    private String urlGamesradar;

    private Double scoreIgn;
    private String urlIgn;

    private Integer scorePcgamer;
    private String urlPcgamer;

    private Double priceSteam;
    private String urlSteam;

    private Double priceGog;
    private String urlGog;

    private Double priceGamersgate;
    private String urlGamersgate;
    
    private Double scoreVideogamer;
    private String urlVideogamer;
    
    private boolean featured;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public void setPlatform(String platform) {
        this.platform = Platform.valueOf(platform);
    }

    public Integer getScoreAltogamer() {
        return scoreAltogamer;
    }

    public void setScoreAltogamer(Integer scoreAltogamer) {
        this.scoreAltogamer = scoreAltogamer;
    }

    public Integer getScoreMetacritic() {
        return scoreMetacritic;
    }

    public void setScoreMetacritic(Integer scoreMetacritic) {
        this.scoreMetacritic = scoreMetacritic;
    }

    public String getUrlMetacritic() {
        return urlMetacritic;
    }

    public void setUrlMetacritic(String urlMetacritic) {
        this.urlMetacritic = urlMetacritic;
    }

    public Double getScoreDestructoid() {
        return scoreDestructoid;
    }

    public void setScoreDestructoid(Double scoreDestructoid) {
        this.scoreDestructoid = scoreDestructoid;
    }

    public String getUrlDestructoid() {
        return urlDestructoid;
    }

    public void setUrlDestructoid(String urlDestructoid) {
        this.urlDestructoid = urlDestructoid;
    }

    public Double getScoreGamesradar() {
        return scoreGamesradar;
    }

    public void setScoreGamesradar(Double scoreGamesradar) {
        this.scoreGamesradar = scoreGamesradar;
    }

    public String getUrlGamesradar() {
        return urlGamesradar;
    }

    public void setUrlGamesradar(String urlGamesradar) {
        this.urlGamesradar = urlGamesradar;
    }

    public Double getScoreGamespot() {
        return scoreGamespot;
    }

    public void setScoreGamespot(Double scoreGamespot) {
        this.scoreGamespot = scoreGamespot;
    }

    public String getUrlGamespot() {
        return urlGamespot;
    }

    public void setUrlGamespot(String urlGamespot) {
        this.urlGamespot = urlGamespot;
    }

    public Double getScoreIgn() {
        return scoreIgn;
    }

    public void setScoreIgn(Double scoreIgn) {
        this.scoreIgn = scoreIgn;
    }

    public String getUrlIgn() {
        return urlIgn;
    }

    public void setUrlIgn(String urlIgn) {
        this.urlIgn = urlIgn;
    }

    public Integer getScorePcgamer() {
        return scorePcgamer;
    }

    public void setScorePcgamer(Integer scorePcgamer) {
        this.scorePcgamer = scorePcgamer;
    }

    public String getUrlPcgamer() {
        return urlPcgamer;
    }

    public void setUrlPcgamer(String urlPcgamer) {
        this.urlPcgamer = urlPcgamer;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Double getPriceSteam() {
        return priceSteam;
    }

    public void setPriceSteam(Double price) {
        this.priceSteam = price;
    }

    public String getUrlSteam() {
        return urlSteam;
    }

    public void setUrlSteam(String urlSteam) {
        this.urlSteam = urlSteam;
    }

    public Double getPriceGamersgate() {
        return priceGamersgate;
    }

    public void setPriceGamersgate(Double priceGamersgate) {
        this.priceGamersgate = priceGamersgate;
    }

    public String getUrlGamersgate() {
        return urlGamersgate;
    }

    public void setUrlGamersgate(String urlGamersgate) {
        this.urlGamersgate = urlGamersgate;
    }

    public Double getPriceGog() {
        return priceGog;
    }

    public void setPriceGog(Double priceGog) {
        this.priceGog = priceGog;
    }

    public String getUrlGog() {
        return urlGog;
    }

    public void setUrlGog(String urlGog) {
        this.urlGog = urlGog;
    }

    public Double getScoreVideogamer() {
        return scoreVideogamer;
    }

    public void setScoreVideogamer(Double scoreVideogamer) {
        this.scoreVideogamer = scoreVideogamer;
    }

    public String getUrlVideogamer() {
        return urlVideogamer;
    }

    public void setUrlVideogamer(String urlVideogamer) {
        this.urlVideogamer = urlVideogamer;
    }
    
    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getImageUrlCover() {
        return imageUrlCover;
    }

    public void setImageUrlCover(String imageUrlCover) {
        this.imageUrlCover = imageUrlCover;
    }
}
