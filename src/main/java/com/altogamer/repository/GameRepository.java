/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. *//* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.repository;

import org.springframework.data.repository.CrudRepository;
import com.altogamer.domain.Game;
import com.altogamer.domain.Platform;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * Data repository for Game domain object.
 */
public interface GameRepository extends CrudRepository<Game, Long>, JpaRepository<Game, Long> {

    Game getByAliasAndPlatform(String alias, Platform platform);

    // In MySQL, string comparisons are case insensitive by default, so there is no need to lower() the fields.
    @Query(value = "select * from game where name like %?1% order by length(name), release_date desc limit 20", nativeQuery = true)
    List<Game> findByKeywords(String keywords);

    @Query(value = "from Game game where game.releaseDate < ?1 and (game.scoreGamespot != null or game.scoreMetacritic != null or game.scoreIgn != null or game.scoreGamesradar != null or game.scorePcgamer != null or game.scoreDestructoid != null)")
    List<Game> findByScoreNotNullAndReleaseDateLessThan(Date releaseDate, Pageable pageable);

    List<Game> findByFeaturedTrue();

    List<Game> findByPublisher(String publisher, Pageable pageable);

    @Query(value = "from Game game where game.releaseDate > ?1 and game.releaseDate < ?2")
    List<Game> findByReleaseDate(Date start, Date end, Pageable pageable);

    @Modifying
    @Query(value = "update Game game set featured = false")
    void updateAllToFeaturedFalse();

    Game findByAlias(String alias);
}
