/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.service;

import com.altogamer.AbstractDatabaseTest;
import com.altogamer.SecurityTestUtils;
import com.altogamer.domain.Game;
import com.altogamer.domain.GameSynonym;
import com.altogamer.repository.GameRepository;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class GameSynonymServiceTest extends AbstractDatabaseTest {

    @Autowired
    private GameSynonymService gameSynonymService;
    @Autowired
    private GameRepository gameRepository;


    @Test
    public void addSynonym_validSynonym_returnGameSynonymCreated() {
        SecurityTestUtils.loginAdminUser();
        String alias = "borderlands-2";
        String synonym = "borderlands-two";
        GameSynonym gameSynonymToCreate = new GameSynonym();
        gameSynonymToCreate.setAlias(alias);
        gameSynonymToCreate.setSynonym(synonym);

        GameSynonym gameSynonymCreated = gameSynonymService.addSynonym(gameSynonymToCreate);

        Game gameDeleted = gameRepository.findByAlias(synonym);

        assertNotNull(gameSynonymCreated);
        assertNull(gameDeleted);
        assertEquals(gameSynonymToCreate.getAlias(), gameSynonymCreated.getAlias());
        assertEquals(gameSynonymToCreate.getSynonym(), gameSynonymCreated.getSynonym());
    }

}
