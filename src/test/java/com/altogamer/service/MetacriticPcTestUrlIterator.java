/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.service;

import com.altogamer.grc.Url;
import com.altogamer.grc.impl.metacritic.MetacriticPcAllUrlIterator;

/**
 * Iterator for all PC games at Metacritic, with a given index limit.
 * Useful for testing. If the page index is grcater than indexLimit, then a
 * large index number is used to force empty results.
 */
public class MetacriticPcTestUrlIterator extends MetacriticPcAllUrlIterator {

    private int indexLimit;

    public MetacriticPcTestUrlIterator(int indexLimit) {
        this.indexLimit = indexLimit;
    }

    @Override
    public synchronized Url next() {
        Url url = super.next();
        if (index > indexLimit) {
            index = 9999;
        }
        return url;
    }

}
