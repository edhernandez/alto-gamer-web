/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer;

import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.test.jdbc.JdbcTestUtils;


/**
 * Initial test file for testing environment.
 * This test can be removed once everything is up and running.
 */
public class SchemaTest extends AbstractDatabaseTest {


    @Test
    public void testSchemaCreation() {
        int count = JdbcTestUtils.countRowsInTable(jdbcTemplate, "game");
        assertTrue(count > 0);
    }

}
