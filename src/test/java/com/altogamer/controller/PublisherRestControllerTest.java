/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.altogamer.controller;

import static org.junit.Assert.*;

import com.altogamer.AbstractDatabaseTest;
import com.altogamer.domain.Publisher;
import com.altogamer.vo.PageVo;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.ModelMap;

public class PublisherRestControllerTest extends AbstractDatabaseTest {

    @Autowired
    private PublisherRestController publisherController;

    @Test
    public void showAll_withPublishersInDataBase_returnView() {
        ModelMap model = new ModelMap();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setContextPath("altogamer.com");
        
        PageVo pageVo = publisherController.showAll(model, 0, request);
        
        List<Publisher> publishers = pageVo.getItems();

        assertNotNull(publishers);
        assertEquals(2, publishers.size());
        assertEquals("2K Games", publishers.get(0).getName());
        assertEquals("altogamer.com/api/publishers?page=1", pageVo.getNextPageUrl());
    }
    
}
