--------------------------------------------------------------------------------
-- General utility SQL querys.
--------------------------------------------------------------------------------

-- select count(*) from game where score_gamespot is not null and score_metacritic is not null and score_ign is not null
-- select count(*) from game where score_metacritic is not null
-- select count(*) from game where score_gamespot is not null
-- select count(*) from game where score_ign is not null
-- select count(*) from game
-- select * from game where lower(name) like '%Orcs Must Die%';

--
-- Get and delete games that were not updated in the last update
--
-- select * from game where last_updated < DATE_SUB(NOW(), INTERVAL 12 HOUR)
-- delete from game where last_updated < DATE_SUB(NOW() , INTERVAL 12 HOUR)