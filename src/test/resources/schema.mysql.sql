--------------------------------------------------------------------------------
-- Schema creation script for production database.
--------------------------------------------------------------------------------

drop table if exists game;
drop table if exists authority;
drop table if exists user;
drop table if exists publisher;
drop table if exists game_synonym;

create table user (
    id bigint auto_increment primary key,
    username varchar(50) unique not null,
    password varchar(100) not null,
    enabled boolean not null
);

create table authority (
    id bigint auto_increment primary key,
    id_user bigint not null,
    authority varchar(50) not null,
    constraint fk_authorities_user foreign key(id_user) references user(id)
);
create unique index ix_auth_user on authority(id_user,authority);

create table game (
    id bigint auto_increment primary key,
    name varchar(200) not null,
    alias varchar(200) not null,
    platform varchar(10) not null,
    publisher varchar(100),
    image_url_cover varchar(200),
    release_date datetime,
    score_altogamer tinyint,
    score_metacritic tinyint,
    url_metacritic varchar(200),
    score_destructoid decimal(3,1),
    url_destructoid varchar(200),
    score_gamespot decimal(3,1),
    url_gamespot varchar(200),
    score_gamesradar tinyint,
    url_gamesradar varchar(200),
    score_ign decimal(3,1),
    url_ign varchar(200),
    score_pcgamer tinyint,
    url_pcgamer varchar(200),
    price_steam decimal(6,2),
    url_steam varchar(200),
    price_gamersgate decimal(6,2),
    url_gamersgate varchar(200),
    price_gog decimal(6,2),
    url_gog varchar(200),
    score_videogamer decimal(3,1),
    url_videogamer varchar(200),
    featured boolean default false not null,
    last_updated datetime not null
);
create unique index ix_alias_platform on game(alias, platform);

create table publisher (
    id bigint auto_increment primary key,
    name varchar(200) not null,
    image_url_cover varchar(200)
);
create unique index ix_publisher on publisher(name);

create table game_synonym (
    id bigint auto_increment primary key,
    alias varchar(200) not null,
    synonym varchar(200) not null
);
create unique index ix_synonym on game_synonym(synonym);

--
-- Test data for development environment.
-- Password for user admin is "adminadmin".
--
insert into user(id, username, password, enabled) values (1, 'admin', 'd8e48551c3bf62dc3aeaa1f3735bbc3867f088700730bc59f145c7b546f7988e2ca318395e9a9f6a', true);
insert into authority (id, id_user, authority) values (1, 1, 'admin');


-- insert into game (name, alias, release_date, platform, score_metacritic, url_metacritic, last_updated) values ('Borderlands', 'borderlands', STR_TO_DATE('12-06-2010', '%d-%m-%Y'),  'PC', 81, 'http://www.metacritic.com/game/pc/borderlands', current_date);
-- insert into game (name, alias, release_date, platform, score_metacritic, url_metacritic, last_updated) values ('FarCry 3',    'farcry-3',    STR_TO_DATE('2-03-2013', '%d-%m-%Y'), 'PC', 88, 'http://www.metacritic.com/game/pc/far-cry-3', current_date);
-- insert into game (name, alias, release_date, platform, score_metacritic, url_metacritic, last_updated) values ('Risen',       'risen',       STR_TO_DATE('1-01-2008', '%d-%m-%Y'),  'PC', 77, 'http://www.metacritic.com/game/pc/risen', current_date);
-- insert into game (`name`, `alias`, platform, publisher, release_date, score_metacritic, url_metacritic, score_destructoid, url_destructoid, score_gamespot, url_gamespot, score_ign, url_ign, last_updated) values ('Borderlands 2', 'borderlands-2', 'PC', NULL, '2010-06-12 00:00:00.0', 81, 'http://www.metacritic.com/game/pc/borderlands', NULL, NULL, NULL, NULL, NULL, NULL, '2013-10-28 00:00:00.0');
-- insert into game (`name`, `alias`, platform, publisher, release_date, score_metacritic, url_metacritic, score_destructoid, url_destructoid, score_gamespot, url_gamespot, score_ign, url_ign, last_updated) values ('Risen 2: Dark Waters', 'risen-2-dark-waters', 'PC', NULL, '2010-06-12 00:00:00.0', 81, 'http://www.metacritic.com/game/pc/borderlands', NULL, NULL, NULL, NULL, NULL, NULL, '2013-10-28 00:00:00.0');
-- insert into game (`name`, `alias`, platform, publisher, release_date, score_metacritic, url_metacritic, score_destructoid, url_destructoid, score_gamespot, url_gamespot, score_ign, url_ign, last_updated) values ('Battlefield 4', 'battlefield-4', 'PC', NULL, '2010-06-12 00:00:00.0', 81, 'http://www.metacritic.com/game/pc/borderlands', NULL, NULL, NULL, NULL, NULL, NULL, '2013-10-28 00:00:00.0');
-- insert into game (`name`, `alias`, platform, publisher, release_date, score_metacritic, url_metacritic, score_destructoid, url_destructoid, score_gamespot, url_gamespot, score_ign, url_ign, last_updated)	values ('Battlefield 3', 'battlefield-3', 'PC', NULL, '2010-06-12 00:00:00.0', 81, 'http://www.metacritic.com/game/pc/borderlands', NULL, NULL, NULL, NULL, NULL, NULL, '2013-10-28 00:00:00.0');
-- insert into game (`name`, `alias`, platform, publisher, release_date, score_metacritic, url_metacritic, score_destructoid, url_destructoid, score_gamespot, url_gamespot, score_ign, url_ign, last_updated)	values ('Thomas was alone', 'thomas-was-alone', 'PC', NULL, '2010-06-12 00:00:00.0', 81, 'http://www.metacritic.com/game/pc/borderlands', NULL, NULL, NULL, NULL, NULL, NULL, '2013-10-28 00:00:00.0');