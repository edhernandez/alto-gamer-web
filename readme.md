# Alto Gamer
##### A game reviews website for gamers, by gamers.

## What is Alto Gamer?
Alto Gamer is a game reviews website for gamers, created by gamers.

You can enjoy Alto Gamer at [www.altogamer.com](http://www.altogamer.com)

## License
Alto Gamer is distributed under the Mozilla Public License, version 2.0.
The LICENSE file contains more information about the licesing of this product.
You can read more about the MPL at Mozilla Public License FAQ.